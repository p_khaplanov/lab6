function updatePrice() {
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }

  function click1() {
 let f1 = document.getElementsByName("field1");
 let r = document.getElementById("result");
if ( isNaN(f1[0].value))  alert("Введенное значение не является числом. Введите данные правильно");
     prodPrice.innerHTML = f1[0].value * price;
return false;
}
  

  let radioDiv = document.getElementById("radios");
  if (select.value == "1") {
      radioDiv.style.display = "none";
    }
    else {
      radioDiv.style.display = "block";
    }
  // Смотрим какая товарная опция выбрана.
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });



  // Скрываем или показываем чекбоксы.
  let checkDiv = document.getElementById("checkboxes");
   if (select.value == "1" || select.value == "2") {
      checkDiv.style.display = "none";
    }
    else if(select.value == "3") {
      checkDiv.style.display = "block";
    }
  // Смотрим какие товарные свойства выбраны.
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice ;
      }
    }
  });

  
  
  let f1 = document.getElementsByName("field1");
  f1.forEach(function(text) {

        let countPrice = f1[0].value;
        let prodPrice = document.getElementById("prodPrice");  
        price = price * countPrice ;
        prodPrice.innerHTML = price + " рублей";
        
    });
     
}
  


function getPrices() {
  return {
    prodTypes: [180, 210, 170],
    prodOptions: {
      option1: 20,
      option2: 0,
      option3: 30,
    },
    prodProperties: {
      prop1: 10,
      prop2: 15,
    }
  };
}

window.addEventListener('DOMContentLoaded', function (event) {
  // Скрываем радиокнопки.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  // Назначаем обработчик на изменение select.
  select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });
  
  // Назначаем обработчик радиокнопок.  
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      updatePrice();
    });

    let f1 = document.getElementsByName("field1");
   f1.forEach(function(text) {
    text.addEventListener("change", function(event) {
        let t = event.target;
        console.log(t.value);
      updatePrice();
          
    });
});
  });


    // Назначаем обработчик радиокнопок.  
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      updatePrice();
    });
  });



  updatePrice();
});